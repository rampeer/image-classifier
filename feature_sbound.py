import Image, ImageDraw
import math
import mymath

"""Figure area / bounding box area"""
class FeatureSBound:
    def area(self, (x1, y1), (x2, y2)):
        return (x1*y2 - y1*x2)/2
    
    def feature(self, img, hull):
        width, height = img.size
        pixels = img.load()
        bb = mymath.BBox()
        bb.calculate(pixels, width, height)

        p = 0.0
        for x in range(0, len(hull)):
            p+=self.area(hull[x-1], hull[x])
        
        return p / ((bb.maxx-bb.minx)*(bb.maxy-bb.miny))
