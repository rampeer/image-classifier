import Image, ImageDraw
import glob
from process import *
import multiprocessing as mp
"""Collects statistics; output file is 'stats.csv'"""

def Collect(itype, q):
    for filename in glob.glob(itype+"/*.BMP"):
        q.put((itype, filename, ProcessImage(filename)))

if __name__ == '__main__':
    f = open('stats.csv', 'w')
    #CSV header
    f.write('"class","weight"')
    for value in FeatureNames():
        f.write(","+value)
    f.write("\n")
    
    #CSV data
    q = mp.Queue()
    p1 = mp.Process(target=Collect, args=("rectangle", q,))
    p2 = mp.Process(target=Collect, args=("circle", q,))
    p3 = mp.Process(target=Collect, args=("triangle", q,))
    p1.start()
    p2.start()
    p3.start()

    while (p1.is_alive() | p2.is_alive() | p3.is_alive()):
        if (q.empty()==False):
            val = q.get()
            iclass, fname, features = val
            print(iclass, fname);
            if (fname.find("special")==-1):
                weight = 1
            else:
                weight = 100
            f.write(str(iclass))
            f.write(","+str(weight))
            for fname in FeatureNames():
                f.write(","+str(features[fname]))
            f.write("\n") 
