import Image, ImageDraw
import math
import mymath

class FeatureDiag:
    def feature(self, img, hull):
        width, height = img.size
        pixels = img.load()
        bb = mymath.BBox()
        bb.calculate(pixels, width, height)

        lens = []
        lens.append(mymath.length(bb.left[0], bb.upper[0]))
        lens.append(mymath.length(bb.upper[1], bb.right[0]))
        lens.append(mymath.length(bb.left[1], bb.lower[0]))
        lens.append(mymath.length(bb.lower[1], bb.right[1]))
        if (max(lens)==0):
            return 1
        else:
            return min(lens) / max(lens)
