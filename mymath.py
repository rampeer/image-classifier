import Image, ImageDraw
import math

def area((x1, y1), (x2, y2)):
    return (x1*y2 - y1*x2)/2

def length((x1, y1), (x2, y2)):
    return math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

def anglecos((x1,y1), (x2,y2)):
    return (x1*x2+y1*y2) / math.sqrt(x1*x1+y1*y1) / math.sqrt(x2*x2+y2*y2)

class BBox:
    upper = []
    lower = []
    left = []
    right = []

    minx = 0
    maxx = 0
    miny = 0
    maxy = 0
    def calculate(self, pixels, width, height):
        minx = width
        miny = height
        maxx = 0
        maxy = 0
        
        for x in range(0, width):
            for y in range(0, height):
                if (pixels[x, y] == 0):
                    minx = min(minx, x)
                    miny = min(miny, y)
                    maxx = max(maxx, x)
                    maxy = max(maxy, y)

        self.minx = minx
        self.miny = miny
        self.maxx = maxx
        self.maxy = maxy
        
        self.left = [(minx, maxy), (minx, miny)]
        self.right = [(maxx, maxy), (maxx, miny)]
        for y in range(0, height):
            if (pixels[self.left[0][0], y] == 0):
                self.left[0] = (self.left[0][0], min(self.left[0][1], y))
            if (pixels[self.left[1][0], y] == 0):
                self.left[1] = (self.left[1][0], max(self.left[1][1], y))
            if (pixels[self.right[0][0], y] == 0):
                self.right[0] = (self.right[0][0], min(self.right[0][1], y))
            if (pixels[self.right[1][0], y] == 0):
                self.right[1] = (self.right[1][0], max(self.right[1][1], y))
        self.upper = [(maxx, miny), (minx, miny)]
        self.lower = [(maxx, maxy), (minx, maxy)]
        for x in range(0, width):
            if (pixels[x, self.upper[0][1]] == 0):
                self.upper[0] = (min(self.upper[0][0], x), self.upper[0][1])
            if (pixels[x, self.upper[1][1]] == 0):
                self.upper[1] = (max(self.upper[1][0], x), self.upper[1][1])
            if (pixels[x, self.lower[0][1]] == 0):
                self.lower[0] = (min(self.lower[0][0], x), self.lower[0][1])
            if (pixels[x, self.lower[1][1]] == 0):
                self.lower[1] = (max(self.lower[1][0], x), self.lower[1][1])
