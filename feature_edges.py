import Image, ImageDraw
import math
from mymath import anglecos, length

"""Counts edges metric (sum of sin^2) of given shape"""
class FeatureEdges:
    thresoldmin = 0
    thresoldmax = 0.9
    
    def feature(self, img, hull):
        width, height = img.size

        vertex = []
        lens = []
        m = 0
        for i in range(0, len(hull)):
            vertex.append((hull[i][0] - hull[i-1][0], hull[i][1] - hull[i-1][1]))
            lens.append(math.sqrt(vertex[i][0]*vertex[i][0] + vertex[i][1]*vertex[i][1]))
            
        lent = 10
        error = 1
        
        for i in range(0, len(hull)):
            acos = anglecos(vertex[i-1], vertex[i])
            if (lens[i] > lent):
                acos = min(error, acos)
                error = 1
                if (acos<self.thresoldmin):
                    m = m + 1
                else:
                    if (acos<self.thresoldmax):
                        m = m + (1 - acos*acos)
            else:
                error = min(error, acos)
                
        if (error<self.thresoldmax):
            if (error<self.thresoldmin):
                m = m + 1
            else:
                m = m + (1 - error*error)
                
        return m
