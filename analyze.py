import glob
import csv
import sys
import Image, ImageDraw
from sklearn import tree
from process import *

#Reads statistics file and fills feature table (table[]) and class column (iclass)
infile = open('stats.csv', 'r')
table = []
for row in csv.reader(infile):
    table.append(row)
table.pop(0)
infile.close()

iclass = []
weights = []

for row in table:
    iclass.append(row.pop(0))
    weights.append(row.pop(0))

#Creates decision tree
clf = tree.DecisionTreeClassifier()
clf = clf.fit(table, iclass, sample_weight=weights)

#Prepares input
if (len(sys.argv)==1):
    args = []
    value = input("File name/pattern: ")
    args.append(value)
else:
    args = sys.argv

#Processes files
for arg in args:
    for filename in glob.glob(arg):
        print(filename)
        #Gets features; sorts them as listed in FeatureNames
        features = ProcessImage(filename)
        data = []
        for featurename in FeatureNames():
            data.append(features[featurename])
        print(clf.predict(data))
