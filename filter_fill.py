import Image, ImageDraw
import numpy
from scipy.spatial import ConvexHull

"""Fill filter; calculates convex hull, fills & returns it"""
class FilterFill:
    def filter(self, img, m_step = 2, m_thresold = 5):
        width, height = img.size
        pixels = img.load()
        black = []
        
        for i in range(width):
            for j in range(height):
                if (pixels[i, j]==0):
                    pixels[i, j] = 255
                    black.append( (i, j) )
            
        hull = ConvexHull(black)
        black = []
        for c in hull.vertices:
            black.append((hull.points[c, 0], hull.points[c, 1]));

        draw = ImageDraw.Draw(img)
        draw.polygon(black, outline=0, fill=0)
        del draw
        
        return black
