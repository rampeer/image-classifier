import Image, ImageDraw
import numpy

"""Median filter using FFT"""
class FilterMedian:
    step = 0
    thresold = 0
    def fold(self, x, y, w, h):
        if (((((x + w/2) % w) - w/2) ** 2 +
             (((y + h/2) % h) - h/2) ** 2) < self.step2):
            return 1
        else:
            return 0
    
    def filter(self, img, m_step = 2, m_thresold = 4, m_count = 1):
        self.step = m_step
        self.step2 = m_step * m_step
        self.thresold = m_thresold*255
        width, height = img.size
        pixels = img.load()
        
        window = [[self.fold(i, j, width, height) for i in range(width)] for j in range(height)]
        cwindow = numpy.fft.fft2(window)
        
        data = [[(255-pixels[i, j]) for i in range(width)] for j in range(height)]
        for k in range(m_count):
            cdata = numpy.fft.fft2(data)
            result = numpy.fft.ifft2(cdata * cwindow)
            for i in range(width):
                for j in range(height):
                    if (result[j, i].real < self.thresold):
                        if (pixels[i, j]==0):
                            data[j][i] = 0
                            pixels[i, j] = 255
