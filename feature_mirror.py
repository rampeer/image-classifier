import Image, ImageDraw
import math
import numpy

"""Calculates mirror metric using FFT"""
def mmax(matrix):
    r = -1
    for x in matrix:
        r=max(r, max(x))
    return r
    
def msum(matrix):
    r = 0
    for x in matrix:
        r=r + sum(x)
    return r

class FeatureMirror:
    
    def feature(self, img, hull):
        width, height = img.size
        pixels = img.load()
        
        data = [[1-pixels[i, j]/255 for i in range(width)] for j in range(height)]
        xdata = [[1-pixels[i, height-j-1]/255 for i in range(width)] for j in range(height)]
        ydata = [[1-pixels[width-i-1, j]/255 for i in range(width)] for j in range(height)]
        xydata = [[1-pixels[i, j]/255 for i in range(width)] for j in range(height)]
        cdata = numpy.fft.fft2(data)
        cxdata = numpy.fft.fft2(xdata)
        cydata = numpy.fft.fft2(ydata)
        cxydata = numpy.fft.fft2(xydata)

        count = msum(data)
        rx = numpy.fft.ifft2(cdata * cxdata)
        ry = numpy.fft.ifft2(cdata * cydata)
        rxy = numpy.fft.ifft2(cdata * cxydata)
        result = mmax(rx).real + mmax(ry).real + mmax(rxy).real
        return (result / count / 3)
