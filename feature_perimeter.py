import Image, ImageDraw
import math
from mymath import area, length

"""Calculates area/perimeter^2 metric"""
class FeaturePerimeter:
    def feature(self, img, hull):
        width, height = img.size

        p = 0.0
        s = 0.0
        for x in range(0, len(hull)):
            p += length(hull[x-1], hull[x])
            s += area(hull[x-1], hull[x])
        return s/p/p
