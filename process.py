import Image, ImageDraw
import glob

from filter_bicomp import FilterBicomp
from filter_median import FilterMedian
from filter_fill import FilterFill

from feature_edges import FeatureEdges
from feature_perimeter import FeaturePerimeter
from feature_mirror import FeatureMirror
#from feature_diag import FeatureDiag
from feature_sbound import FeatureSBound
from feature_sbound2 import FeatureSBound2


bicomp = FilterBicomp()
median = FilterMedian()
ffill = FilterFill()

edges = FeatureEdges()
perim = FeaturePerimeter()
mirror = FeatureMirror()
#diag = FeatureDiag()
sbound = FeatureSBound()
sbound2 = FeatureSBound2()

"""Filters image and returns array of features"""
def ProcessImage(name, debug_save = False):
    img = Image.open(name).convert("1")
    median.filter(img, 2, 1, 1)
    bicomp.filter(img, 2)
    median.filter(img, 2, 1, 1)
    hull = ffill.filter(img)
    if (debug_save):
        img.save(name + "_test.bmp")
    
    result = { }
    result["edges"] = edges.feature(img, hull)
    result["perimeter"] = perim.feature(img, hull)
    result["mirror"] = mirror.feature(img, hull)
    result["sbound"] = sbound.feature(img, hull)
    result["sbound2"] = sbound2.feature(img, hull)

    return result

"""Returns registered feature names"""
def FeatureNames():
    return ["edges", "perimeter", "mirror", "sbound", "sbound2"]
