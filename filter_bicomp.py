import Image, ImageDraw

"""Bicomponent filter; finds largest black pixel island"""
class FilterBicomp:
    step = 2
    color = []
    corners = 1
    width, height = (0, 0)
    pixels = 0
    
    def floodfill(self, img, color, PivotX, PivotY):
        count = 1
        lookup = []
        lookup.append((PivotX, PivotY))
        while (len(lookup)>0):
            current = lookup.pop()
            if ((self.color[current[0]][current[1]]!=0) | (self.pixels[current[0], current[1]]!=0)):
                continue
            
            self.color[current[0]][current[1]] = color
            count+=1
            for x in range(max(current[0] - self.step, 0), min(current[0] + self.step, self.width)):
                for y in range(max(current[1] - self.step, 0), min(current[1] + self.step, self.height)):
                    if (self.corners):
                        lookup.append((x,y))
                    else:
                        if (((x-current[0])^2 + (y-current[1])^2) < ((self.step-1)*(self.step-1))):
                            lookup.append((x,y))
        return count

    def filter(self, img, m_step = 2, m_corners = 1):
        self.step = m_step
        self.corners = m_corners
        islands = []
        currentcolor = 1
        
        self.width, self.height = img.size
        self.pixels = img.load()
        self.color = [[0 for i in range(self.height)] for j in range(self.width)]
        for x in range(0, self.width): 
            for y in range(0, self.height):
                if (self.pixels[x, y]==0):
                    count = self.floodfill(img, currentcolor, x, y)
                    if (count>0):
                        islands.append((count, currentcolor, x, y))
                        currentcolor+=1
                        
        islands.sort(key=lambda dat: -dat[0])
        for x in range(0, self.width):
            for y in range(0, self.height):
                if (self.color[x][y]!=islands[0][1]):
                    self.pixels[x, y] = 255
