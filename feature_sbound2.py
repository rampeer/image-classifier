import Image, ImageDraw
import math
import mymath

"""Calculates (Figure area / bounded area) metric"""
class FeatureSBound2:
    def feature(self, img, hull):
        width, height = img.size
        pixels = img.load()
        bb = mymath.BBox()
        bb.calculate(pixels, width, height)

        p = 0.0
        for x in range(0, len(hull)):
            p+=mymath.area(hull[x-1], hull[x])

        p2 = 0.0
        p2 += mymath.area(bb.lower[0], bb.left[1])
        p2 += mymath.area(bb.left[1], bb.left[0])
        p2 += mymath.area(bb.left[0], bb.upper[0])
        p2 += mymath.area(bb.upper[0], bb.upper[1])
        p2 += mymath.area(bb.upper[1], bb.right[0])
        p2 += mymath.area(bb.right[0], bb.right[1])
        p2 += mymath.area(bb.right[1], bb.lower[1])
        p2 += mymath.area(bb.lower[1], bb.lower[0])
        
        return p / p2
