import Image, ImageDraw

"""Median filter"""
class FilterMedian:
    step = 3
    thresold = 12
    color = []
    width, height = (0, 0)
    pixels = 0

    def count(self, cx, cy):
        cnt = 0
        for x in range(cx-self.step, cx+self.step):
            for y in range(cy-self.step, cy+self.step):
                if ((x in range(0, self.width)) &
                    (y in range(0, self.height))):
                    if (self.pixels[x, y] == 0):
                        cnt+=1
                else:
                    cnt+=1
        return cnt
    
    def filter(self, img, m_step = 3, m_thresold = 12):
        self.step = m_step
        self.thresold = m_thresold
        img = img.convert("1")
        ignore = []
        
        self.width, self.height = img.size
        self.pixels = img.load()
        self.color = [[0 for i in range(self.height)] for j in range(self.width)]
        iterations = 0
        black = []
        for x in range(0, self.width): 
            for y in range(0, self.height):
                if (self.pixels[x, y]==0):
                    black.append((x, y))
        while (1):
            for c in black:
                if (self.count(c[0], c[1]) < self.thresold):
                    ignore.append(c)
            for (x,y) in ignore:
                black.remove((x,y))
                self.pixels[x, y] = 255
            print(len(ignore))
            if (len(ignore)==0):
                break
            else:
                ignore = []
            iterations+=1
            
        #img.save("temp2.bmp")
        return img

#f = FilterMedian()
#f.filter(Image.open("test5.bmp"))

